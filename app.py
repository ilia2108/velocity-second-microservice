from typing import Text
from flask import Flask, request, jsonify

from flask.wrappers import Response

import json


app = Flask(__name__)

value = "sample value"

@app.route("/")
def home():
    return 'Second microservice'

@app.route("/api/v1/getText", methods=['GET'])
def get_text():
    print(value)
    return jsonify({'text': value})

@app.route("/api/v1/setText", methods=['POST'])
def set_text():
    #print(request.json['text'])
    content = request.json
    print(content)
    value =  content['text']
    return jsonify({'newText': value})

if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=5001, debug=True)